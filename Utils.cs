using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ants.Jobs;

namespace Ants
{
	public static class Utils
	{
		public static readonly Random Random;

		static Utils()
		{
			Random = new Random();
		}

		internal static bool IsDoubleEqual(double d1, double d2)
		{
			double difference = Math.Abs(d1 * .00001);
			return Math.Abs(d1 - d2) <= difference;
		}

		public static string GetDescription(this Enum genericEnum)
		{
			Type genericEnumType = genericEnum.GetType();
			MemberInfo[] memberInfo = genericEnumType.GetMember(genericEnum.ToString());

			if ((memberInfo.Length <= 0)) return genericEnum.ToString();

			object[] attributes =
				memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);

			return attributes.Any()
				? ((System.ComponentModel.DescriptionAttribute)attributes.ElementAt(0)).Description
				: genericEnum.ToString();
		}

		public static T RandomElement<T>(this IEnumerable<T> source)
		{
			IEnumerable<T> enumerable = source as T[] ?? source.ToArray();
			return !enumerable.Any()
				? default
				: enumerable.Skip((int)(Utils.Random.NextDouble() * enumerable.Count() - 1)).First();
		}

		public static CSGBox CreateOverlayBox(Vector3 location, Color color)
		{
			CSGBox newBox = new CSGBox()
			{
				Width = 1,
				Height = 1,
				Depth = 1,
				Transform = new Transform(Quat.Identity, location),
			};
			newBox.Material = new SpatialMaterial()
			{
				AlbedoColor = color,
			};

			return newBox;
		}

		public static IntVector[] Get26Neighbourhood(IntVector position)
		{
			List<IntVector> result = new List<IntVector>();
			for (int x = -1; x < 2; x++)
			{
				for (int y = -1; y < 2; y++)
				{
					for (int z = -1; z < 2; z++)
					{
						if (!(x == 0 && y == 0 && z == 0))
						{
							result.Add(new IntVector(position.x + x, position.y + y, position.z + z));
						}
					}
				}
			}

			return result.ToArray();
		}

		public static IntVector[] Get6Neighbourhood(IntVector position)
		{
			List<IntVector> result = new List<IntVector>()
			{
				new IntVector(position.x + 1, position.y, position.z),
				new IntVector(position.x - 1, position.y, position.z),
				new IntVector(position.x, position.y + 1, position.z),
				new IntVector(position.x, position.y - 1, position.z),
				new IntVector(position.x, position.y, position.z + 1),
				new IntVector(position.x, position.y, position.z - 1),
			};

			return result.ToArray();
		}

		/// <summary>
		/// Given an IntVector position, and a unit direction, calculates all the positions where
		/// a vector pointing from position to direction would not have to rotate more than 45 degrees to point there
		/// </summary>
		/// <param name="position"></param>
		/// <param name="direction"></param>
		/// <returns></returns>
		public static IntVector[] GetDirectionNeighbourhood(IntVector position, IntVector direction)
		{
			IntVector[] neighbourHood = Get26Neighbourhood(position);
			if (!direction.IsUnitDirection())
			{
				throw new InvalidOperationException("direction needs to be a unit direction vector");
			}

			IntVector[] targets = Get6Neighbourhood(position + direction);
			IntVector[] directions = targets
				.Intersect(neighbourHood)
				.Select(d => d - position)
				.Concat(new IntVector[] { direction }).ToArray();

			return directions;
		}

		public static Job GetNewJobInstance(this JobEnum enumValue)
		{
			return Job.JobFromEnumValue(enumValue);
		}
	}
}