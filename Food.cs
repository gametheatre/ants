using Godot;

namespace Ants
{
	public class Food : RigidBody
	{
		public float Energy { get; set; }
		public bool IsCarried { get; set; }

		public void OnCollision(Node collided)
		{
			if (collided is Ant ant && !this.IsCarried)
			{
				ant.FoodFound(this);
			}
		}
	}
}