using Godot;
using System;
using Ants.Jobs;
using Ants.Pheremones;

namespace Ants
{
	public class Ant : KinematicBody
	{
		protected const float Gravity = -98f;

		protected readonly float Speed = 100;
		protected readonly double DigDownChance = 0.1;

		protected Vector3 Velocity;
		protected Random Random;
		protected TimeSpan ActionRate;
		protected Timer ActionTimer;
		protected float DigSize = 2f;
		protected float[] LastLocationPheremones;

		protected bool WasOutside = false;

		public Colony Colony { get; set; }

		//public JobEnum CurrentJobEnum { get; set; } = JobEnum.None;
		public Job CurrentJob { get; set; } = null;
		public bool ShowNavigation { get; set; }
		public Node Carrying { get; protected set; }
		public Vector3 MovingDirection { get; set; } = Vector3.Zero;
		public float[] Status { get; set; }

		public IntVector VoxelPosition
		{
			get { return new IntVector(this.Transform.origin); }
		}

		// Called when the node enters the scene tree for the first time.
		public override void _Ready()
		{
			this.Velocity = new Vector3(0, 0, 0);
			this.Random = new Random(this.GetHashCode());
			this.ActionRate = TimeSpan.FromSeconds(1);
			this.ActionTimer = new Timer(this.ActionRate);
			this.Status = new float[Enum.GetValues(typeof(PheremoneEnum)).Length];
		}

		public override void _PhysicsProcess(float delta)
		{
			this.Move(this.MovingDirection, delta);
		}

		public override void _Process(float delta)
		{
			if (this.CurrentJob == null)
			{
				this.Colony.GetNewJob(this);
			}

			if (this.ActionTimer.CheckAndReset())
			{
				this.DoAction();
			}
		}

		public void FoodFound(Food food)
		{
			if (this.CurrentJob is ScoutJob)
			{
				this.Colony.GetNewJob(this, JobEnum.GetFood);
			}

			this.Status = new[] { 0f, 0f, PheremoneMap.StatusBase, 0f };
			this.Carrying = food;
			food.IsCarried = true;
			this.AddChild(food);
		}

		protected virtual void DoAction()
		{
			this.CurrentJob.DoAction(this);

			this.LastLocationPheremones ??= this.Colony.PheremoneMap.GetValue(new IntVector(this.Transform.origin));

			Pheremone.ProcessAll(
				this.Colony.PheremoneMap,
				new IntVector(this.Transform.origin),
				this.LastLocationPheremones,
				this.Status,
				this.IsOutside(),
				this.WasOutside);

			this.LastLocationPheremones = this.Colony.PheremoneMap.GetValue(new IntVector(this.Transform.origin));
		}

		protected void Move(Vector3 direction, float delta)
		{
			this.Velocity = direction * this.Speed * delta;
			this.Velocity.y = Gravity * delta;
			this.MoveAndSlide(this.Velocity, Vector3.Up);
		}

		public void DoDig()
		{
			if (this.Random.NextDouble() < 0.2)
			{
				if (this.Random.NextDouble() < this.DigDownChance)
				{
					this.MovingDirection = Vector3.Down;
				}
			}

			this.DoDig(this.MovingDirection);
		}

		protected void DoDig(Vector3 direction)
		{
			Vector3 digLocation = this.Transform.origin + direction * this.Scale +
			                      new Vector3(0, this.DigSize * 1.5f - this.Scale.x, 0);
			Terrain.CurrentInstance.PaintSphere(digLocation, this.DigSize, false);
			this.Colony.AddTunnel(digLocation);
		}

		public bool IsOutside()
		{
			return this.Colony.IsTunnel(this.VoxelPosition);
		}
	}
}