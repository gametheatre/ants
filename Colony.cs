using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using Ants.Jobs;
using Ants.Pheremones;

namespace Ants
{
	public class Colony : Node
	{
		private Dictionary<IntVector, bool> tunnels;
		private int memberCap = 200;
		private int tunnelCap = 1000;
		private int brood = 0;
		private int workers = 0;
		private int queens = 1;
		private int Members => this.brood + this.workers + this.queens;
		private int desiredMemberCount = 10;
		private Node lastVisualisationNode;

		public int TunnelCount = 0;
		public int DesiredTunnelCount = 50;
		public PheremoneMap PheremoneMap { get; private set; }
		public double PopulationRatio => this.Members / (float)this.desiredMemberCount;
		public double[] JobPriorities { get; private set; }
		public int[] CurrentJobs { get; private set; }
		public IntVector Entrance { get; set; }

		// Called when the node enters the scene tree for the first time.
		public override void _Ready()
		{
			this.tunnels = new Dictionary<IntVector, bool>();
			this.PheremoneMap = new PheremoneMap();
			this.JobPriorities = Enumerable.Repeat(0.0, Enum.GetNames(typeof(JobEnum)).Length).ToArray();
			this.JobPriorities[0] = 0;
			this.JobPriorities[2] = 100;
			this.CurrentJobs = new int[Enum.GetNames(typeof(JobEnum)).Length];
		}

		internal void AddMember(Node newMember)
		{
			switch (newMember)
			{
				case Brood broodMember:
					broodMember.Colony = this;
					this.brood++;
					break;
				case Queen queen:
					queen.Colony = this;
					this.queens++;
					break;
				case Ant ant:
					ant.Colony = this;
					this.workers++;
					break;
			}
		}

		internal void RemoveMember(Node toRemove)
		{
			switch (toRemove)
			{
				case Brood:
					this.brood--;
					break;
				case Queen:
					this.queens--;
					break;
				case Ant ant:
					this.workers--;
					break;
			}
		}

		public bool AddTunnel(Vector3 position)
		{
			if (this.IsTunnel(position)) return false;
			this.tunnels.Add(new IntVector(position), true);
			this.TunnelCount++;
			return true;
		}

		public bool IsTunnel(Vector3 position)
		{
			this.tunnels.TryGetValue(new IntVector(position), out bool isTunnel);
			return isTunnel;
		}

		public bool IsTunnel(IntVector position)
		{
			this.tunnels.TryGetValue(position, out bool isTunnel);
			return isTunnel;
		}

		public void GetNewJob(Ant ant, JobEnum opportunity = JobEnum.None)
		{
			int jobResult = 0;
			if (ant.CurrentJob != null)
			{
				this.CurrentJobs[(int)ant.CurrentJob.EnumValue]--;
			}

			if (opportunity != JobEnum.None)
			{
				//There may need to be some constraints here
				jobResult = (int)opportunity;
			}
			else
			{
				double sum = this.JobPriorities.Sum();
				double needed = 0;

				for (int i = 0; i < this.JobPriorities.Length; i++)
				{
					double desiredWorkers = this.JobPriorities[i] / sum * this.workers;
					double currentNeeded = desiredWorkers - this.CurrentJobs[i];
					if (!(currentNeeded > needed)) continue;
					jobResult = i;
					needed = currentNeeded;
				}
			}

			this.CurrentJobs[jobResult]++;
			ant.CurrentJob = ((JobEnum)jobResult).GetNewJobInstance();

			// Debug
			// if (this.CurrentJobs[jobResult] == 1)
			// {
			// ant.ShowNavigation = true;
			// }

			GD.Print("Assigned job " + jobResult);
		}

		public void OnDesiredTunnelsChanged(float value)
		{
			this.DesiredTunnelCount = (int)(value * this.tunnelCap / 100);
		}

		public void OnDesiredMembersChanged(float value)
		{
			this.desiredMemberCount = (int)(value * this.memberCap / 100);
		}

		#region UITRIGGERS

		public void OnJobPrioritiesChanged(float value, int job)
		{
			this.JobPriorities[job] = value;
		}

		public void VisualisePheremoneMap(PheremoneEnum p)
		{
			List<Node> cubes = this.PheremoneMap.Visualise(p);
			this.AddOverlayCubes(cubes);
		}

		public void HighlightMapTile(Vector3 location, Color color)
		{
			CSGBox newBox = Utils.CreateOverlayBox(location, color);
			this.AddOverlayCubes(new List<Node> { newBox }, false);
		}

		private void AddOverlayCubes(List<Node> cubes, bool clearPrevious = true)
		{
			if (this.lastVisualisationNode != null && clearPrevious)
			{
				this.RemoveChild(this.lastVisualisationNode);
				this.lastVisualisationNode = null;
			}

			this.lastVisualisationNode ??= new Node();

			foreach (Node cube in cubes)
			{
				this.lastVisualisationNode.AddChild(cube);
			}

			if (clearPrevious)
			{
				this.AddChild(this.lastVisualisationNode);
			}
		}

		#endregion
	}
}