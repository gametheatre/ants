using Godot;
using System;

namespace Ants
{
	public class Timer
	{
		public TimeSpan Duration {get; set;}
		public DateTime Since { get; private set; }
		public Timer() : this(TimeSpan.FromSeconds(1))
		{
			
		}
		public Timer(TimeSpan duration)
		{
			this.Duration = duration;
			this.Since = DateTime.Now;
		}

		public bool CheckAndReset()
		{
			return this.CheckAndReset(this.Duration);
		}

		public bool CheckAndReset(TimeSpan duration)
		{
			this.Duration = duration;
			if(this.Check(duration))
			{
				this.Reset();
				return true;
			}
			return false;
		}

		private bool Check()
		{
			return this.Check(this.Duration);
		}

		private bool Check(TimeSpan duration)
		{
			return duration >= TimeSpan.Zero && duration < DateTime.Now - this.Since;
		}

		public void Reset()
		{
			this.Since = DateTime.Now;
		}
	}
}
