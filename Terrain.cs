using Godot;
using System;

namespace Ants
{
	public class Terrain : VoxelLodTerrain
	{
		public const int MapSize = 1024;

		public static Terrain CurrentInstance;
		private VoxelTool vt;
		private Random random;

		// Called when the node enters the scene tree for the first time.
		public override void _Ready()
		{
			this.random = new Random();
			CurrentInstance = this;
			this.vt = this.GetVoxelTool();
		}

		public void PaintSphere(Vector3 position, float radius, bool add)
		{
			if (add)
			{
				this.vt.Mode = VoxelTool.ModeEnum.Add;
				this.vt.Value = 1;
			}
			else
			{
				this.vt.Mode = VoxelTool.ModeEnum.Remove;
				this.vt.Value = 1;
			}

			this.vt.DoSphere(position, radius);
		}

		public Vector3 GetRandomSurfaceLocation()
		{
			int x = (int)(this.random.NextDouble() * MapSize - (MapSize / 2f));
			int z = (int)(this.random.NextDouble() * MapSize - (MapSize / 2f));
			Vector3 origin = new(x, (MapSize / 2f), z);
			VoxelRaycastResult result = this.vt.Raycast(origin, Vector3.Down, MapSize);
			return result.Position;
		}
	}
}