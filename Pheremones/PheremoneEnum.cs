namespace Ants.Pheremones
{
	public enum PheremoneEnum
	{
		Entrance,
		Danger,
		FoodSource,
		Searched,
	}
}