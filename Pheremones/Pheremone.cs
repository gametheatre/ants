using System;
using System.Collections.Generic;
using System.Linq;

namespace Ants.Pheremones
{
	public abstract class Pheremone
	{
		private static readonly Pheremone[] pheremones;

		public abstract int Index { get; }
		public abstract PheremoneEnum EnumValue { get; }
		public abstract bool IsStatusRelevant { get; }


		/// <returns>The new status for this ant</returns>
		public virtual float[] Process(
			PheremoneMap map,
			IntVector location,
			float lastValue,
			float[] status,
			bool isOutside,
			bool wasOutside)
		{
			float[] newStatus = status.ToArray();
			if (!this.IsStatusRelevant) return newStatus;

			// If we left a status trail, continue spreading it at the last rate seen
			if (lastValue > PheremoneMap.MinimumPerceivable && newStatus[this.Index] <= PheremoneMap.MinimumPerceivable)
			{
				newStatus[this.Index] =
					Math.Min(lastValue / PheremoneMap.StatusGradientFactor, PheremoneMap.StatusBase);
			}

			float amount = newStatus[this.Index] * PheremoneMap.StatusGradientFactor;
			if (amount > 0)
			{
				newStatus[this.Index] -= amount;
				map.AddPheremone(location, this.EnumValue, amount);
			}


			return newStatus;
		}

		static Pheremone()
		{
			Pheremone[] pheremonesList = new Pheremone[]
				{ new Entrance(), new Danger(), new FoodSource(), new Searched() };
			pheremones = new Pheremone[Enum.GetValues(typeof(PheremoneEnum)).Length];
			foreach (Pheremone pheremone in pheremonesList)
			{
				pheremones[pheremone.Index] = pheremone;
			}
		}

		public static Pheremone Get(int i)
		{
			if (i >= 0 && i < pheremones.Length)
			{
				return pheremones[i];
			}

			return null;
		}

		public static Pheremone Get(PheremoneEnum e)
		{
			int i = (int)e;
			if (i >= 0 && i < pheremones.Length)
			{
				return pheremones[i];
			}

			return null;
		}

		/// <returns>The new status for this ant</returns>
		public static float[] ProcessAll(
			PheremoneMap map,
			IntVector location,
			float[] lastValue,
			IEnumerable<float> status,
			bool isOutside,
			bool wasOutside)
		{
			float[] newStatus = status.ToArray();

			return pheremones.Aggregate(newStatus, (current, pheremone)
				=>
			{
				if (pheremone != null)
				{
					return pheremone.Process(map, location, lastValue[pheremone.Index], current, isOutside, wasOutside);
				}

				throw new InvalidOperationException("Missing pheremone implementation");
			});
		}
	}
}