namespace Ants.Pheremones
{
	public class Danger : Pheremone
	{
		public override int Index => (int)PheremoneEnum.Danger;
		public override PheremoneEnum EnumValue => PheremoneEnum.Danger;
		public override bool IsStatusRelevant => true;
	}
}