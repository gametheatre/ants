namespace Ants.Pheremones
{
	public class Searched : Pheremone
	{
		public override int Index => (int)PheremoneEnum.Searched;
		public override PheremoneEnum EnumValue => PheremoneEnum.Searched;
		public override bool IsStatusRelevant => true;

		public override float[] Process(
			PheremoneMap map,
			IntVector location,
			float lastValue,
			float[] status,
			bool isOutside,
			bool wasOutside)
		{
			if (status[this.Index] > 0)
			{
				map.AddPheremone(location, this.EnumValue, status[this.Index] * PheremoneMap.StatusGradientFactor);
			}

			return status;
		}
	}
}