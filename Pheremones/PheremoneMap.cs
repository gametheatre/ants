using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

namespace Ants.Pheremones
{
	public class PheremoneMap
	{
		//Pheremone decay rate per in game minute
		private const float DecayRate = 0.001f;
		public const float MinimumPerceivable = 0.00001f;
		public const float StatusBase = 10f;
		public const float StatusGradientFactor = 0.1f;
		private readonly Dictionary<IntVector, Tuple<float[], DateTime>> map;
		private readonly int numPheremones;
		private List<Node> visualiseNodes;

		public static float DefaultEmit { get; set; } = 0.5f;

		public PheremoneMap()
		{
			this.numPheremones = Enum.GetNames(typeof(PheremoneEnum)).Length;
			this.map = new Dictionary<IntVector, Tuple<float[], DateTime>>();
			this.visualiseNodes = new List<Node>();
		}

		public float GetValue(IntVector position, PheremoneEnum pheremone)
		{
			return this.map.ContainsKey(position) ? this.UpdateAndReturn(position)[(int)pheremone] : 0f;
		}

		public float[] GetValue(IntVector position)
		{
			return this.UpdateAndReturn(position);
		}

		public void AddPheremone(IntVector position, PheremoneEnum pheremone, float value)
		{
			if (this.map.ContainsKey(position))
			{
				this.UpdateAndReturn(position);
			}

			if (!this.map.ContainsKey(position))
			{
				this.map[position] = new Tuple<float[], DateTime>(new float[this.numPheremones], DateTime.Now);
			}

			this.map[position].Item1[(int)pheremone] += value;
		}

		public List<Node> Visualise(PheremoneEnum p, float sensitivity = 10f)
		{
			this.visualiseNodes.Clear();
			foreach (KeyValuePair<IntVector, Tuple<float[], DateTime>> location in this.map)
			{
				this.visualiseNodes.Add(Utils.CreateOverlayBox(
					location.Key.ToVector3(),
					new Color(Math.Min(location.Value.Item1[(int)p] / sensitivity, 1), 0, 0, 1)
				));
			}

			return this.visualiseNodes;
		}

		public override string ToString()
		{
			return string.Join(",",
				this.map.Select(l => $"{{{l.Key.ToString()}}}, {{{string.Join(",", l.Value.Item1)}}}"));
		}

		private float[] UpdateAndReturn(IntVector position)
		{
			if (this.map.ContainsKey(position))
			{
				DateTime oldTimestamp = this.map[position].Item2;
				bool hasValue = false;
				for (int i = 0; i < this.numPheremones; i++)
				{
					float value = this.map[position].Item1[i];
					value = Math.Max(0,
						value - (value * DecayRate * (float)(DateTime.Now - oldTimestamp).TotalMilliseconds / 60000));
					if (value > 0)
					{
						hasValue = true;
					}

					this.map[position].Item1[i] = value;
				}

				if (hasValue) return this.map[position].Item1;
				this.map.Remove(position);
				return new float[this.numPheremones];
			}
			else
			{
				return new float[this.numPheremones];
			}
		}
	}
}