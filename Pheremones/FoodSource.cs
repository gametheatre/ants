namespace Ants.Pheremones
{
	public class FoodSource : Pheremone
	{
		public override int Index => (int)PheremoneEnum.FoodSource;
		public override PheremoneEnum EnumValue => PheremoneEnum.FoodSource;
		public override bool IsStatusRelevant => true;
	}
}