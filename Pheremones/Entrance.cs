using System.Linq;

namespace Ants.Pheremones
{
	public class Entrance : Pheremone
	{
		public override int Index => (int)PheremoneEnum.Entrance;
		public override PheremoneEnum EnumValue => PheremoneEnum.Entrance;
		public override bool IsStatusRelevant => true;

		public override float[] Process(
			PheremoneMap map,
			IntVector location,
			float lastValue,
			float[] status,
			bool isOutside,
			bool wasOutside)
		{
			float[] newStatus = status.ToArray();
			if (isOutside != wasOutside)
			{
				newStatus[(int)PheremoneEnum.Entrance] = PheremoneMap.StatusBase;
			}
			else
			{
				newStatus = base.Process(map, location, lastValue, newStatus, isOutside, wasOutside);
			}

			return newStatus;
		}
	}
}