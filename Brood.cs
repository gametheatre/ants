using Godot;
using System;

namespace Ants
{
	public class Brood : RigidBody
	{
		PackedScene antScene = (PackedScene)GD.Load("res://Ant.tscn");
		static TimeSpan[] durations;
		public int currentPhase = 0;
		DateTime lastTransformation;
		public Colony Colony;

		public override void _Ready()
		{
			durations = new TimeSpan[] { TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2) };
			this.lastTransformation = DateTime.Now;
		}

		public override void _Process(float delta)
		{
			if (this.currentPhase >= 3)
			{
				return;
			}

			if (durations[this.currentPhase] < DateTime.Now - this.lastTransformation)
			{
				this.lastTransformation = DateTime.Now;
				this.currentPhase++;
				CollisionShape shape = (CollisionShape)this.GetNode("CollisionShape");
				MeshInstance mesh = (MeshInstance)this.GetNode("MeshInstance");
				shape.Transform = shape.Transform.Scaled(new Vector3(1, 1.2f, 1));
				mesh.Transform = shape.Transform.Scaled(new Vector3(1, 1.2f, 1));
				if (this.currentPhase >= 3)
				{
					KinematicBody newAnt = (KinematicBody)this.antScene.Instance();
					newAnt.Transform = new Transform(Quat.Identity, this.Transform.origin);
					this.Colony.RemoveMember(this);
					this.Colony.AddMember(newAnt);
					this.GetParent().AddChild(newAnt);
					this.GetParent().RemoveChild(this);
					this.QueueFree();
				}
			}
		}
	}
}
