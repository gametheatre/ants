using System.Collections.Generic;
using Godot;
using System.Linq;
using Ants.Pheremones;

namespace Ants
{
	public static class Navigation
	{
		/// <summary>
		/// Chooses a navigation direction, avoiding negative pheremones, following positive ones
		/// preferring not diverging from the original travel direction by more than 45 degrees
		/// </summary>
		/// <param name="map"></param>
		/// <param name="location"></param>
		/// <param name="positivePheremones"></param>
		/// <param name="negativePheremones"></param>
		/// <param name="lastTrajectory"></param>
		/// <returns></returns>
		// Please note: internally this function uses direction vectors with +1 added to each element
		// This is to make them match up with 3d (3x3x3) neighbourhood array indices.
		// Do not use these coordinates as normal vectors without calling either ToMapLocation or subtracting 1 from each element
		public static Vector3 Navigate(
			PheremoneMap map,
			Vector3 location,
			PheremoneEnum[] positivePheremones,
			PheremoneEnum[] negativePheremones,
			Vector3 lastTrajectory
		)
		{
			/*GD.Print(map.ToString());
			GD.Print(location);*/
			float[,,] resultsNeighbourhood = new float[,,]
			{
				{
					{ 0f, 0f, 0f }, { 0f, 0f, 0f }, { 0f, 0f, 0f }
				},
				{
					{ 0f, 0f, 0f }, { 0f, 0f, 0f }, { 0f, 0f, 0f }
				},
				{
					{ 0f, 0f, 0f }, { 0f, 0f, 0f }, { 0f, 0f, 0f }
				}
			};

			for (int x = 0; x < 3; x++)
			{
				// Temp ignore vertical
				for (int y = 1; y < 2; y++)
				{
					for (int z = 0; z < 3; z++)
					{
						float[] mapPheremones = map.GetValue(ToMapLocation(location, x, y, z));

						foreach (PheremoneEnum pheremone in positivePheremones)
						{
							resultsNeighbourhood[x, y, z] += mapPheremones[(int)pheremone];
						}


						foreach (PheremoneEnum pheremone in negativePheremones)
						{
							resultsNeighbourhood[x, y, z] -= mapPheremones[(int)pheremone];
						}
					}
				}
			}

			List<IntVector> maxDirections = new List<IntVector>();

			float maxValue = float.NegativeInfinity;
			for (int x = 0; x < 3; x++)
			{
				// Temp ignore vertical
				for (int y = 1; y < 2; y++)
				{
					for (int z = 0; z < 3; z++)
					{
						if (x == 0 && y == 0 && z == 0)
						{
							continue;
						}

						if (IsClose(resultsNeighbourhood[x, y, z], maxValue))
						{
							IntVector toAdd = new IntVector(x, y, z);
							if (!maxDirections.Contains(toAdd))
							{
								maxDirections.Add(toAdd);
							}

							continue;
						}

						if (resultsNeighbourhood[x, y, z] > maxValue)
						{
							maxValue = resultsNeighbourhood[x, y, z];
							maxDirections.Clear();
							maxDirections.Add(new IntVector(x, y, z));
						}
					}
				}
			}

			IEnumerable<IntVector> toSelect = maxDirections;
			IntVector lastDirection = new IntVector(lastTrajectory);
			if (!lastDirection.Equals(new IntVector(0, 0, 0)))
			{
				IEnumerable<IntVector> preferredDirections = Utils.GetDirectionNeighbourhood(
					new IntVector(location),
					lastDirection
				).Select(d => new IntVector(d.x + 1, d.y + 1, d.z + 1));

				List<IntVector>
					preferredMaxDirections = preferredDirections.Intersect(maxDirections).ToList();
				toSelect = preferredMaxDirections.Any() ? preferredMaxDirections : maxDirections;
			}

			IntVector randomised = toSelect.RandomElement();

			Vector3 result = ToMapLocation(location, randomised).ToVector3();
			return result;
		}

		private static bool IsClose(float a, float b)
		{
			if (float.IsNaN(a))
			{
				return float.IsNaN(b);
			}

			return a < b + PheremoneMap.MinimumPerceivable && a > b - PheremoneMap.MinimumPerceivable;
		}

		private static IntVector ToMapLocation(Vector3 location, IntVector direction)
		{
			return ToMapLocation(location, direction.x, direction.y, direction.z);
		}

		private static IntVector ToMapLocation(Vector3 location, int x, int y, int z)
		{
			return new IntVector(new Vector3(location.x + x - 1, location.y + y - 1, location.z + z - 1));
		}
	}
}