using System;
using Ants.Pheremones;
using Godot;
using Array = Godot.Collections.Array;

namespace Ants.Jobs
{
	public class JobsUI : GridContainer
	{
		private readonly NodePath colonyPath = "/root/WorldEnvironment/Colony";
		private Colony colony;

		public override void _Ready()
		{
			this.colony = (Colony)this.GetNode(this.colonyPath);
			for (int i = 0; i < this.colony.JobPriorities.Length; i++)
			{
				JobEnum jobEnum = (JobEnum)i;
				if (jobEnum == default(JobEnum))
				{
					continue;
				}

				string name = Enum.GetName(typeof(JobEnum), jobEnum);
				Label label = new Label();
				label.Name = name + "Label";
				label.Text = jobEnum.GetDescription();
				this.AddChild(label);
				HSlider slider = new HSlider();
				slider.Name = name + "Slider";
				slider.Value = this.colony.JobPriorities[i];
				slider.MaxValue = 100;
				slider.MinValue = 0;
				Array args = new Godot.Collections.Array();
				args.Add((int)jobEnum);
				slider.Connect("value_changed", this.colony, nameof(Colony.OnJobPrioritiesChanged), args);
				this.AddChild(slider);
			}
		}

		public void OnSearchedButtonPressed()
		{
			this.colony.VisualisePheremoneMap(PheremoneEnum.Searched);
		}

		public void OnFoodSourceButtonPressed()
		{
			this.colony.VisualisePheremoneMap(PheremoneEnum.FoodSource);
		}
	}
}