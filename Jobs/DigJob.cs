using Ants.Pheremones;

namespace Ants.Jobs
{
	public class DigJob : Job
	{
		public override JobEnum EnumValue => JobEnum.Dig;

		public override void DoAction(Ant ant)
		{
			if (ant.IsOutside())
			{
				ant.MovingDirection = Navigation.Navigate(
					ant.Colony.PheremoneMap,
					ant.Transform.origin,
					new[] { PheremoneEnum.Entrance },
					new[] { PheremoneEnum.Danger },
					ant.MovingDirection
				);
				return;
			}

			ant.MovingDirection = Navigation.Navigate(
				ant.Colony.PheremoneMap,
				ant.Transform.origin,
				new PheremoneEnum[] { },
				new[] { PheremoneEnum.Danger, PheremoneEnum.Entrance },
				ant.MovingDirection
			);
			if (ant.Colony.TunnelCount < ant.Colony.DesiredTunnelCount)
			{
				ant.DoDig();
			}
		}
	}
}