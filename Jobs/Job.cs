using System;
using System.Linq;

namespace Ants.Jobs
{
	public abstract class Job
	{
		public abstract JobEnum EnumValue { get; }

		public virtual void DoAction(Ant ant)
		{
			// Do nothing
		}

		public static Job JobFromEnumValue(JobEnum enumValue)
		{
			switch (enumValue)
			{
				case JobEnum.None:
					return null;
				case JobEnum.Dig:
					return new DigJob();
				case JobEnum.GetFood:
					return new GetFoodJob();
				case JobEnum.BroodCare:
					// TODO
					break;
				case JobEnum.Patrol:
					// TODO
					break;
				case JobEnum.Scout:
					return new ScoutJob();
				case JobEnum.Fight:
					// TODO
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(enumValue), enumValue, null);
			}

			return null;
		}
	}
}