using System.ComponentModel;
using System.Linq;

namespace Ants.Jobs
{
	public enum JobEnum
	{
		[Description("None")] None,
		[Description("Dig")] Dig,
		[Description("Get food")] GetFood,
		[Description("Care for the Brood")] BroodCare,
		[Description("Patrol")] Patrol,
		[Description("Scout")] Scout,
		[Description("Fight")] Fight,
	}
}