using Ants.Pheremones;
using Godot;

namespace Ants.Jobs
{
	public class GetFoodJob : Job
	{
		public override JobEnum EnumValue => JobEnum.GetFood;

		public override void DoAction(Ant ant)
		{
			ant.Status[(int)(PheremoneEnum.Searched)] = PheremoneMap.StatusBase;
			Vector3 targetPosition;
			if (!ant.IsOutside())
			{
				targetPosition = Navigation.Navigate(
					ant.Colony.PheremoneMap,
					ant.Transform.origin,
					new[] { PheremoneEnum.Entrance },
					new[] { PheremoneEnum.Danger },
					ant.MovingDirection
				);

				ant.MovingDirection = (ant.Transform.origin.DirectionTo(targetPosition)).Normalized();
			}
			else
			{
				targetPosition = Navigation.Navigate(
					ant.Colony.PheremoneMap,
					ant.Transform.origin,
					new[] { PheremoneEnum.FoodSource },
					new[] { PheremoneEnum.Danger, PheremoneEnum.Entrance, PheremoneEnum.Searched },
					ant.MovingDirection
				);
				ant.MovingDirection = (ant.Transform.origin.DirectionTo(targetPosition)).Normalized();
			}

			if (ant.ShowNavigation)
			{
				ant.Colony.HighlightMapTile(targetPosition, Colors.Green);
			}
		}
	}
}