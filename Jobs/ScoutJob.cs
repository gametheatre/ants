using Ants.Pheremones;
using Godot;

namespace Ants.Jobs
{
	public class ScoutJob : Job
	{
		public override JobEnum EnumValue => JobEnum.Scout;

		public override void DoAction(Ant ant)
		{
			ant.Status[(int)(PheremoneEnum.Searched)] = PheremoneMap.StatusBase;
			if (!ant.IsOutside())
			{
				ant.MovingDirection = Navigation.Navigate(
					ant.Colony.PheremoneMap,
					ant.Transform.origin,
					new[] { PheremoneEnum.Entrance },
					new[] { PheremoneEnum.Danger },
					ant.MovingDirection
				);
				return;
			}

			Vector3 targetPosition = Navigation.Navigate(
				ant.Colony.PheremoneMap,
				ant.Transform.origin,
				new PheremoneEnum[] { },
				new[] { PheremoneEnum.Danger, PheremoneEnum.Entrance, PheremoneEnum.Searched },
				ant.MovingDirection
			);
			ant.MovingDirection = (ant.Transform.origin.DirectionTo(targetPosition)).Normalized();

			if (ant.ShowNavigation)
			{
				ant.Colony.HighlightMapTile(targetPosition, Colors.Green);
			}
		}
	}
}