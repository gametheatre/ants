using Godot;

namespace Ants
{
	public class World : Node
	{
		private readonly PackedScene genericFoodScene = (PackedScene)GD.Load("res://GenericFood.tscn");

		public override void _Ready()
		{
			base._Ready();
			for (int i = 0; i < 10; i++)
			{
				Vector3 location = Terrain.CurrentInstance.GetRandomSurfaceLocation();
				for (int j = 0; j < 10; j++)
				{
					SpawnInstance(location, this.genericFoodScene);
				}
			}
		}

		private static void SpawnInstance(Vector3 location, PackedScene rigidBody)
		{
			RigidBody newInstance = (RigidBody)rigidBody.Instance();
			newInstance.Transform = new Transform(Quat.Identity, location);
		}
	}
}