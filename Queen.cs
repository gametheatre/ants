using Godot;
using System;
using Ants.Pheremones;

namespace Ants
{
	// ReSharper disable once ClassNeverInstantiated.Global
	public class Queen : Ant
	{
		private readonly PackedScene eggScene = (PackedScene)GD.Load("res://Brood.tscn");
		private const int InitialBurrowSize = 1;
		private int burrowCounter;
		private const double LayingRateSeconds = 10;
		private Timer eggTimer;

		public override void _Ready()
		{
			base._Ready();
			this.MovingDirection = Vector3.Zero;
			this.DigSize = 3f;
			this.eggTimer = new Timer();
			this.Colony = (Colony)this.GetParent();
		}

		public override void _PhysicsProcess(float delta)
		{
			if (this.burrowCounter < InitialBurrowSize)
			{
				this.Move(this.MovingDirection, delta);
			}
		}

		public override void _Process(float delta)
		{
			if (this.ActionTimer.CheckAndReset())
			{
				this.DoAction();
			}
		}

		protected override void DoAction()
		{
			if (this.burrowCounter < InitialBurrowSize)
			{
				this.DigBurrow();
			}
			else if (!Utils.IsDoubleEqual(this.Colony.PopulationRatio, 1.0) &&
			         this.eggTimer.CheckAndReset(
				         TimeSpan.FromSeconds(LayingRateSeconds / (1 - this.Colony.PopulationRatio))))
			{
				RigidBody newEgg = (RigidBody)this.eggScene.Instance();
				newEgg.Transform = new Transform(Quat.Identity, this.Transform.origin + Vector3.Back * this.Scale.x);
				this.Colony.AddMember(newEgg);
				this.GetParent().AddChild(newEgg);
				GD.Print("egg");
			}
		}

		private void DigBurrow()
		{
			if (this.Colony.Entrance == default)
			{
				this.Colony.Entrance = this.VoxelPosition;
			}

			IntVector groundPosition =
				new IntVector(this.VoxelPosition.x, this.VoxelPosition.y - 1, this.VoxelPosition.z);
			this.Colony.PheremoneMap.AddPheremone(groundPosition, PheremoneEnum.Searched,
				PheremoneMap.StatusBase);
			foreach (IntVector position in Utils.Get26Neighbourhood(groundPosition))
			{
				this.Colony.PheremoneMap.AddPheremone(position, PheremoneEnum.Searched, PheremoneMap.StatusBase);
			}

			this.MovingDirection = this.MovingDirection == Vector3.Zero ? Vector3.Down : Vector3.Zero;
			this.DoDig(this.MovingDirection);
			this.burrowCounter++;
		}
	}
}