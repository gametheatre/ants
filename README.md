This is a simulator designed to simulate the activity of an ant colony. This is partially meant as a game and partially
as an experiment to see how viable it is to base a 3D pathfinding system on ant navigation strategies.

Currently this project is not possible to build and run as the fast pace of development on the Godot engine has meant
that the [voxel plugin](https://github.com/Zylann/godot_voxel) I am using is not working in the current version. I
expect once Godot 3.5 development stabilises and the team focuses on Godot 4.0 that the voxel plugin team will get it
working again and then development will continue.