using System;
using Godot;

namespace Ants
{
	public struct IntVector
	{
		public int x { get; set; }
		public int y { get; set; }
		public int z { get; set; }

		public IntVector(Vector3 vec)
		{
			this.x = (int)Math.Round(vec.x);
			this.y = (int)Math.Round(vec.y);
			this.z = (int)Math.Round(vec.z);
		}

		public IntVector(int x, int y, int z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public Vector3 ToVector3()
		{
			return new Vector3(this.x, this.y, this.z);
		}

		public float LengthSquared()
		{
			return this.ToVector3().LengthSquared();
		}

		public bool IsUnitDirection()
		{
			return Math.Abs(this.x) <= 1 && Math.Abs(this.y) <= 1 && Math.Abs(this.z) <= 1;
		}

		public override bool Equals(object obj)
		{
			if (obj is IntVector vec)
			{
				return vec.x == this.x && vec.y == this.y && vec.z == this.z;
			}

			return false;
		}

		//Take the 10 least significant bits of each number. 
		//If the map is larger than 1024*1024*1024 there will be hash collisions
		public override int GetHashCode()
		{
			return (this.x << 20) & (this.y << 20 >> 10) & (this.z << 20 >> 20);
		}

		public override string ToString()
		{
			return $"{this.x}, {this.y}, {this.z}";
		}

		public static bool operator ==(IntVector a, IntVector b)
		{
			return a.x == b.x && a.y == b.y && a.z == b.z;
		}

		public static bool operator !=(IntVector a, IntVector b)
		{
			return !(a == b);
		}

		public static IntVector operator +(IntVector a, IntVector b)
		{
			return new IntVector(a.x + b.x, a.y + b.y, a.z + b.z);
		}

		public static IntVector operator -(IntVector a, IntVector b)
		{
			return new IntVector(a.x - b.x, a.y - b.y, a.z - b.z);
		}
	}
}